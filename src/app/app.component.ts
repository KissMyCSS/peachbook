import { Component } from '@angular/core';
import { ThemeService } from './modules/theme/services/theme.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'peachbook';

  constructor(private themeService: ThemeService) {};

  ngOnInit() {
    this.themeService.init();
  }
}
