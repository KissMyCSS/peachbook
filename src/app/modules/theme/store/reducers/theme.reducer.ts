import { createReducer, on } from "@ngrx/store";
import { Theme, peach } from "../../interfaces/theme.interface";
import { applyTheme } from "../actions/theme.actions";

export interface State {
    theme: Theme;
}

const initialThemeState: State = {
    theme: peach
};

export const themeReducer = createReducer(
    initialThemeState,
    on(applyTheme, (state, { theme }) => ({ theme: theme }))
)