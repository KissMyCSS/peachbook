import { Action, createAction, props } from '@ngrx/store';
import { Theme } from '../../interfaces/theme.interface';

export enum ThemeActions {
    changeTheme = '[Theme] Change',
    applyTheme  = '[Theme] Apply'
}

export const changeTheme = createAction(
    ThemeActions.changeTheme,
    props<{ theme: Theme }>()
)

export const applyTheme = createAction(
    ThemeActions.applyTheme,
    props<{ theme: Theme }>()
)