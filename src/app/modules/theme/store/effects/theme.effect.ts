import { Injectable } from "@angular/core";
import { Actions, createEffect, ofType } from "@ngrx/effects";
import { ThemeService } from "../../services/theme.service";
import { ThemeActions, applyTheme } from "../actions/theme.actions";
import { exhaustMap, of } from "rxjs";

@Injectable()
export class ThemeEffects {

    change$ = createEffect(() =>
        this.actions$.pipe(
            ofType(ThemeActions.changeTheme),
            exhaustMap(action => {
                    this.themeService.setTheme(action);
                    return of(applyTheme({theme: action}));
            })
        )
    )

    constructor(
        private actions$: Actions,
        private themeService: ThemeService
    ) { }
}