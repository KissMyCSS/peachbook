import { Injectable } from "@angular/core";
import { Theme, peach } from "../interfaces/theme.interface";
import { Store } from "@ngrx/store";
import { changeTheme } from "../store/actions/theme.actions";
import { Observable, of } from "rxjs";

@Injectable({
    providedIn: "root"
})
export class ThemeService {
    /**
     * Sets the app color theme to css file
     * @param theme
     */
    setTheme(theme: Theme): void{
        if (!theme) {
            throw new Error(`Faild to set ${theme} as a theme`);
        }

        console.log(theme);
        
        const choosenTheme = theme[theme.choosen];
        
        Object.entries(choosenTheme).forEach(property => {
            document.documentElement.style.setProperty(
                property[0],
                property[1]
            );
        });
    }

    init() {
        //TODO add theme loading with checking wheher is there any theme in lcoalstorage
        this.store.dispatch(changeTheme({ theme: peach }))
    }

    constructor(private store: Store<Theme>) {}
}