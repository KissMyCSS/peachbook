/**
 * Defines color set for app theme
 */
export interface ThemeColors {
    "--theme-background": string;
    "--theme-backgroun-blur": string;

    "--theme-background-menu": string;
    "--theme-nav-hover": string;

    "--theme-text-normal": string;
    "--theme-text-secondary": string;

    "--theme-interactive-normal": string;
    "--theme-interactive-hover": string;
    "--theme-interactive-active": string;
    "--theme-interactive-note-icons": string;
    "--theme-interactive-txt-vector": string;
    "--theme-interactive-dark-text": string;
}

/**
 * Defines app color theme
 */
export interface Theme {
    name: string;
    choosen: "light" | "dark";
    light: ThemeColors,
    dark: ThemeColors
}

/**
 * Default app theme, "Peach"
 */
export const peach: Theme = {
    name: "Peach",
    choosen: "light",
    light: {
        "--theme-background": "#FDFCDC",
        "--theme-backgroun-blur": "rgba(48, 3, 0, 0.6)",
    
        "--theme-background-menu": "#FED9B7",
        "--theme-nav-hover": "#FABFA3",
    
        "--theme-text-normal": "#530B06",
        "--theme-text-secondary": "#C87855",
    
        "--theme-interactive-normal": "#F07167",
        "--theme-interactive-hover": "#FA493B",
        "--theme-interactive-active": "#E96E29",
        "--theme-interactive-note-icons": "#B8352B",
        "--theme-interactive-txt-vector": "#FDF4F0",
        "--theme-interactive-dark-text": "#C4693E"
    },
    dark: {
        "--theme-background": "#30273C",
        "--theme-backgroun-blur": "rgba(20, 0, 46, 0.7)",
    
        "--theme-background-menu": "#261F32",
        "--theme-nav-hover": "#211A2D",
    
        "--theme-text-normal": "#EFD288",
        "--theme-text-secondary": "#EFD288",
    
        "--theme-interactive-normal": "#C84968",
        "--theme-interactive-hover": "#A42946",
        "--theme-interactive-active": "#AB5343",
        "--theme-interactive-note-icons": "#170E28",
        "--theme-interactive-txt-vector": "#F9F0FD",
        "--theme-interactive-dark-text": "#825CB5"
    }
}