import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { ThemeEffects } from './store/effects/theme.effect';
import { themeReducers } from './store/theme.store';



@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    StoreModule.forRoot(themeReducers),
    EffectsModule.forRoot(ThemeEffects)
  ]
})
export class ThemeModule { }
